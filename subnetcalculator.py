# stap 1
def ask_for_number_sequence(message):
    ingevoerde_ip = input(message + "\n")

    return [int(adres) for adres in ingevoerde_ip.split(".")]

# stap 2
def is_valid_ip_address(numberlist):
    Correcte_Input = True

    if len(numberlist) == 4:
        for ip_splitted in numberlist:
            splitted_ip = int(ip_splitted)
            if not (splitted_ip >= 0 and splitted_ip <= 255):
                Correcte_Input = False
    else:
        Correcte_Input = False

    return Correcte_Input

# stap 3
def is_valid_netmask(numberlist):
    correcte_subnetmask = False
    check_ones = True
    binair_subnetmask = ""

    if len(numberlist) == 4:
        correcte_subnetmask = True
        for net_sep in numberlist:
            binair_subnetmask =  binair_subnetmask + f"{int(net_sep):08b}"
           
        for bit in binair_subnetmask:          
            if check_ones ==  True and bit == "1":
                correcte_subnetmask = True
            elif bit == "0" and check_ones == True:
                check_ones = False
                correcte_subnetmask = True
            elif check_ones == False and bit == "1":
                correcte_subnetmask = False
            
    return correcte_subnetmask

# stap 4
def one_bits_in_netmask(Numlist):
    counter = 0
    binary_subnetmask = ""
    
    for bit in Numlist:
            binary_subnetmask += f"{int(bit):08b}"
        
    for bit2 in binary_subnetmask:
        if int(bit2) == 1:
            counter += 1

    return counter 

# stap 5
def apply_network_mask(host_address, netmask):
    Binaire_subnetmask = []
    i = 0
    Getal1 = 0
    Getal2 = 0
    Getal3 = 0
    

    while i < 4:
        Getal1 = f"{int(host_address[i]):08b}"
        Getal2 = f"{int(netmask[i]):08b}"
        Getal3 = f"{int(Getal1,2) & int(Getal2,2):08b}"
        Binaire_subnetmask.append(f"{int(Getal3,2)}")
        i = i + 1
    Getal3=Binaire_subnetmask[0]+"."+Binaire_subnetmask[1]+"."+Binaire_subnetmask[2]+"."+Binaire_subnetmask[3]

    return(Getal3)

# stap 6
def netmask_to_wildcard_mask(Subnetmask):
    wildcard_mask = []
    for element in Subnetmask:
        wildcard_element = ""
        for bit in f"{int(element):08b}":
            if bit == "0":
                wildcard_element += "1"
            else:
                wildcard_element += "0"
        wildcard_element = int(wildcard_element, 2)
        wildcard_mask.append(wildcard_element)

    return(wildcard_mask)

# stap 7
def get_broadcast_address(network_address, wildcard_mask):
    Byte1 = 0
    Byte2 = 0
    Byte3 = 0
    Bytes = 0
    binair_new = []

    while Bytes < 4:
        Byte1 = f"{int(network_address[Bytes]):08b}"
        Byte2 = f"{int(wildcard_mask[Bytes]):08b}"
        Byte3 = f"{int(Byte1,2) | int(Byte2,2):08b}"
        binair_new.append(f"{int(Byte3,2)}")
        Bytes = Bytes + 1
    Byte3 = binair_new[0]+"."+binair_new[1]+"."+binair_new[2]+"."+binair_new[3]

    return(Byte3)

# stap 8
def prefix_length_to_max_hosts(numbers_netmask):
    max_hosts = pow(2, numbers_netmask) - 2
    return(int(max_hosts))

ip_address = ask_for_number_sequence("Wat is het IP-adres?")
netmask_numbers = ask_for_number_sequence("Wat is het subnetmask?")
if is_valid_ip_address(ip_address) and is_valid_netmask(netmask_numbers):
    print("IP-adres en subnetmasker zijn geldig.")
else:
    import sys
    print("IP-adres en/of subnetmasker is ongeldig.")
    sys.exit(0)
netmask_lengte = one_bits_in_netmask(netmask_numbers)
print("De lengte van het subnetmasker is " + str(netmask_lengte) + ".")
print("Het adres van het subnet is " + str(apply_network_mask(ip_address, netmask_numbers)) + ".")
wildcardmasker = netmask_to_wildcard_mask(netmask_numbers)
print("Het wildcardmasker is " + str(wildcardmasker[0]) + "." + str(wildcardmasker[1]) + "." + str(wildcardmasker[2]) + "." + str(wildcardmasker[3]))
print("Het broadcastadres is " + str(get_broadcast_address(ip_address, wildcardmasker)) + ".")
print("Het maximaal aantal hosts op dit subnet is " + str(prefix_length_to_max_hosts(32-netmask_lengte)) + ".")
